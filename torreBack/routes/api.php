<?php

use Illuminate\Http\Request;

Route::group([
    'prefix' => 'organization'
], function () {
    Route::post('strengths', 'OrganizationController@calculateStrengthAvg')->name('organization.strengths');
});
