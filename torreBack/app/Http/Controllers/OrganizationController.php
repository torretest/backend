<?php

namespace App\Http\Controllers;

use App\Http\Services\OrganizationService;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    public function calculateStrengthAvg(Request $request)
    {
        $members = $request->members;
        $OrganizationService = new OrganizationService;
        $OrganizationService->calculateStrengthAvg($members);
        return response()->json([
            $OrganizationService->getResponse()
        ]);
    }
}
