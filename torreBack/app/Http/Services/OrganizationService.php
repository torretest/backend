<?php
namespace App\Http\Services;

use Exception;

class OrganizationService {
    protected $message;
    protected $error;
    protected $data;

    public function calculateStrengthAvg($members){
        try {
            $members = explode(",", $members);
            $numberOfMembers = count($members);
            $weight = 0;
            $personalityAnalisisAvg["openness-to-experience"] = 0;
            $personalityAnalisisAvg["honesty-humility"] = 0;
            $personalityAnalisisAvg["agreeableness"] = 0;
            $personalityAnalisisAvg["conscientiousness"] = 0;
            $personalityAnalisisAvg["emotionality"] = 0;
            $personalityAnalisisAvg["extraversion"] = 0;

            $personalityAnalisisMedia = [3.41, 3.19, 2.94, 3.44, 3.43, 3.5];

            foreach($members as $username){
                $url = "https://bio.torre.co/api/bios/".$username;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, trim($url, " "));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

                $head = curl_exec($ch);
                if (curl_errno($ch)) {
                    $error_msg = curl_error($ch);
                }
                curl_close($ch);

                if (!isset($error_msg)) { // if the api get status 200
                    $jsonApi = json_decode($head, true);
                    $weight += !empty($jsonApi["person"]["weight"]) ? $jsonApi["person"]["weight"] : 0; // validate the weight just in case we get a null
                    if(isset($jsonApi["personalityTraitsResults"])){ // Validation just in case this person doesn't has personality trait
                        $personality = $jsonApi["personalityTraitsResults"];
                        if(isset($personality["analyses"])){
                            $personalityTest = $personality["analyses"];
                            foreach($personalityTest as $score){
                                if(isset($personalityAnalisisAvg[$score["groupId"]] )){
                                    $personalityAnalisisAvg[$score["groupId"]] += $score["analysis"]; // Acumulate by type so we can calculate avg
                                }
                            }
                        }
                    }
                }
            }

            $objMyteam = [];

            foreach ($personalityAnalisisAvg as $key => $sum){ // getting avg
                $personalityAnalisisAvg[$key] =  $sum / $numberOfMembers;
                $objMyteam[] = array(
                    "name" => $key,
                    "value" => $personalityAnalisisAvg[$key]
                );
            }
            $response = Array(
                "weightAvg" => $weight/$numberOfMembers,
                "testAvg" => $objMyteam,
                "testMed" => $personalityAnalisisMedia
            );

            $this->message = "Members avg";
            $this->error = false;
            $this->data = $response;
        } catch (Exception $e) {
            $this->error = true;
            $this->message = "Error getting AVG";
            $this->data = $e;
        }
    }

    public function getResponse()
    {
        $response["error"] = $this->error;
        $response["message"] = $this->message;
        $response["data"] = $this->data;
        return $response;
    }
}
